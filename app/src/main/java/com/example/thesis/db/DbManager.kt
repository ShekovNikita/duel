package com.example.thesis.db

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import androidx.core.app.NotificationManagerCompat.from

class DbManager(val context: Context) {

    val dbHelper = DbHelper(context)
    var db: SQLiteDatabase? = null

    fun openDb() {
        db = dbHelper.writableDatabase
    }

    fun insertPlayerToDb(name: String, countdown: Int, duel: Int) {
        val values = ContentValues().apply {
            put(PlayersClass.COLUMN_NAME, name)
            put(PlayersClass.COLUMN_COUNTDOWN, countdown)
            put(PlayersClass.COLUMN_DUEL_WINS, duel)
        }
        db?.insert(PlayersClass.TABLE_NAME, null, values)
    }

    fun updateCounterTimer(counter: Int, name: String){
        val values = ContentValues().apply {
            put(PlayersClass.COLUMN_COUNTDOWN, counter)
        }
        db?.update(PlayersClass.TABLE_NAME, values, PlayersClass.COLUMN_NAME +" = ?", arrayOf(name))
    }

    fun updateDuelWins(counter: Int, name: String){
        val values = ContentValues().apply {
            put(PlayersClass.COLUMN_DUEL_WINS, counter)
        }
        db?.update(PlayersClass.TABLE_NAME, values, PlayersClass.COLUMN_NAME +" = ?", arrayOf(name))
    }

    @SuppressLint("Range")
    fun readDbPlayersNames(): ArrayList<String> {
        val datalist = ArrayList<String>()
        val cursor = db?.query(
            PlayersClass.TABLE_NAME, null, null, null,
            null, null, null, null
        )
        while (cursor?.moveToNext()!!) {
            val dataName = cursor.getString(cursor.getColumnIndex(PlayersClass.COLUMN_NAME))
            datalist.add(dataName.toString())
        }
        cursor.close()
        return datalist
    }

    @SuppressLint("Range")
    fun readDbPlayersDuelWins(): ArrayList<Int>{
        val datalist = ArrayList<Int>()
        val cursor = db?.query(
            PlayersClass.TABLE_NAME, null, null, null,
            null, null, null, null)
        while (cursor?.moveToNext()!!) {
            val dataName = cursor.getInt(cursor.getColumnIndex(PlayersClass.COLUMN_DUEL_WINS))
            datalist.add(dataName)
        }
        cursor.close()
        return datalist
    }

    @SuppressLint("Range")
    fun readDbPlayersTimerCounter(): ArrayList<Int>{
        val datalist = ArrayList<Int>()
        val cursor = db?.query(
            PlayersClass.TABLE_NAME, null, null, null,
            null, null, null, null)
        while (cursor?.moveToNext()!!) {
            val dataName = cursor.getInt(cursor.getColumnIndex(PlayersClass.COLUMN_COUNTDOWN))
            datalist.add(dataName)
        }
        cursor.close()
        return datalist
    }

    fun closeDb(){
        dbHelper.close()
    }

    fun removeAll(){
        db?.delete(PlayersClass.TABLE_NAME,null,null)
    }
}