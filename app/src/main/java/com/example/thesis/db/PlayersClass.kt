package com.example.thesis.db

import android.provider.BaseColumns

object PlayersClass {
    const val TABLE_NAME = "players"
    const val COLUMN_NAME = "name"
    const val COLUMN_COUNTDOWN = "countdown"
    const val COLUMN_DUEL_WINS = "duel_wins"

    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "ListOfPlayers.db"

    const val CREATE_TABLE = "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
            "${BaseColumns._ID} INTEGER PRIMARY KEY," +
            "$COLUMN_NAME TEXT," +
            "$COLUMN_COUNTDOWN INTEGER," +
            "$COLUMN_DUEL_WINS INTEGER)"

    const val SQL_DELETE_TABLE = "DROP TABLE IF EXISTS $TABLE_NAME"
}