package com.example.thesis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.thesis.db.DbManager

class Duel : AppCompatActivity() {

    private val COUNTER_HEIGHT = 50

    var button1: Button? = null
    var button2: Button? = null

    var winnerActivity: Intent? = null

    val dbManager = DbManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.duel)

        winnerActivity = Intent(this, WinnerActivity::class.java)

        button1 = findViewById(R.id.btn1)
        button2 = findViewById(R.id.btn2)

        dbManager.openDb()
        val playersNames = dbManager.readDbPlayersNames()

    }

    override fun onResume() {
        super.onResume()

        var cout = 0

        button1?.text = dbManager.readDbPlayersNames()[0]
        button1?.setOnClickListener {
            if (cout == 5) {
                winnerActivity?.putExtra("1", dbManager.readDbPlayersNames()[0])
                button1?.isEnabled = false
                button2?.isEnabled = false
                startActivity(winnerActivity)
                button1?.height = button2?.height!!
                dbManager.updateDuelWins(dbManager.readDbPlayersDuelWins()[0] + 1, dbManager.readDbPlayersNames()[0])
            } else {
                button1?.height = button1?.height?.plus(COUNTER_HEIGHT)!!
                button2?.height = button2?.height?.minus(COUNTER_HEIGHT)!!
                cout++
            }
        }

        button2?.text = dbManager.readDbPlayersNames()[1]
        button2?.setOnClickListener {
            if (cout == -5) {
                winnerActivity?.putExtra("2", dbManager.readDbPlayersNames()[1])
                button1?.isEnabled = false
                button2?.isEnabled = false
                startActivity(winnerActivity)
                button1?.height = button2?.height!!
                dbManager.updateDuelWins(dbManager.readDbPlayersDuelWins()[1] + 1, dbManager.readDbPlayersNames()[1])
            } else {
                button2?.height = button2?.height?.plus(COUNTER_HEIGHT)!!
                button1?.height = button1?.height?.minus(COUNTER_HEIGHT)!!
                cout--
            }
        }
    }

    fun playerChoice(listPlayers: ArrayList<String>){

    }
}