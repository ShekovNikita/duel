package com.example.thesis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.thesis.db.DbManager
import java.util.ArrayList

class CountDownTimerMy : AppCompatActivity() {

    val dbManager = DbManager(this)

    var btn_start: Button? = null
    var btn_repeat: Button? = null
    var btn_main: Button? = null
    var btn_countdown: Button? = null

    var tvTimer: TextView? = null
    var player_name: ArrayList<String>? = null
    var name = 0

    var cdam: Intent? = null
    var main: Intent? = null

    var timer: CountDownTimer? = null

    var games = 3


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_count_down_timer)

        dbManager.openDb()

        cdam = Intent(this, CountDownTimerMy::class.java)
        main = Intent(this, MainActivity::class.java)

        btn_countdown = findViewById(R.id.button3)
        player_name = dbManager.readDbPlayersNames()
        btn_repeat = findViewById(R.id.button)
        btn_main = findViewById(R.id.button2)
        tvTimer = findViewById(R.id.chronometer)
        btn_start = findViewById(R.id.button4)

        timer = object : CountDownTimer(1000, 10) {
            override fun onTick(seconds: Long) {
                tvTimer?.text = String.format("%1d:%03d", seconds / 1000, seconds % 1000)
            }

            override fun onFinish() {
                tvTimer?.text = "Ну хватит уже"
                btn_countdown?.isEnabled = false
                btn_repeat?.visibility = View.VISIBLE
                btn_main?.visibility = View.VISIBLE
            }
        }


    }

    override fun onResume() {
        super.onResume()
        println("-----------------------onResume")
        if (name < dbManager.readDbPlayersNames().size) {
            var counter = 0
            btn_countdown?.isEnabled = true
            btn_countdown?.text = "$counter"
            btn_repeat?.visibility = View.GONE
            btn_main?.visibility = View.GONE

            btn_start?.visibility = View.VISIBLE
            btn_start?.text = player_name?.get(name)

            btn_start?.setOnClickListener {
                timer?.start()
                btn_start?.visibility = View.GONE
                btn_countdown?.setOnClickListener {
                    counter++
                    btn_countdown?.text = "$counter"
                }
            }

            btn_repeat?.setOnClickListener {
                dbManager.updateCounterTimer(
                    dbManager.readDbPlayersTimerCounter()[name] + counter,
                    player_name?.get(name)!!
                )
                name++
                onResume()
            }

            btn_main?.setOnClickListener {
                onDestroy()
            }
        }
        else{
            onPause()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        println("-----------------------onDestroy")
        dbManager.closeDb()
        startActivity(main)
    }

    override fun onStop() {
        super.onStop()
        println("-----------------------onStop")
    }

    override fun onPause() {
        super.onPause()
        println("-----------------------OnPause")
        games--
        if (games <= 0){
            onDestroy()
        }
        else {
            name = 0
            onResume()
        }
    }
}