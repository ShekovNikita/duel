package com.example.thesis

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.thesis.db.DbManager


class MainActivity : AppCompatActivity() {

    val dbManager = DbManager(this)

    var update_btn: Button? = null

    var namesText: TextView? = null
    var duelText: TextView? = null
    var timerText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbManager.openDb()

        val duel = Intent(this, Duel::class.java)
        val enterPlayers = Intent(this, EnterPlayers::class.java)
        val cdam = Intent(this, CountDownTimerMy::class.java)

        val duel_btn: Button = findViewById(R.id.duel_btn)
        val countdown_btn: Button = findViewById(R.id.countdown_btn)
        val players_btn: Button = findViewById(R.id.btn_players)
        update_btn = findViewById(R.id.update_btn)

        namesText = findViewById(R.id.namesText)
        duelText = findViewById(R.id.duelText)
        timerText = findViewById(R.id.timerText)

        for (i in dbManager.readDbPlayersNames()) {
            namesText?.append("$i\n")
        }

        for (i in dbManager.readDbPlayersDuelWins()) {
            duelText?.append("$i\n")
        }

        for (i in dbManager.readDbPlayersTimerCounter()) {
            timerText?.append("$i\n")
        }


        duel_btn.setOnClickListener {
            onDestroy()
            startActivity(duel)
        }

        countdown_btn.setOnClickListener {
            onStop()
            startActivity(cdam)
        }

        players_btn.setOnClickListener {
            startActivity(enterPlayers)
            onStop()
        }
    }

    override fun onResume() {
        super.onResume()
        update_btn?.setOnClickListener {
            namesText = null
            duelText = null
            timerText = null
            for (i in dbManager.readDbPlayersNames()) {
                dbManager.updateDuelWins(0, i)
                dbManager.updateCounterTimer(0, i)
                namesText?.append("$i\n")
            }

            for (i in dbManager.readDbPlayersDuelWins()) {
                duelText?.append("$i\n")
            }

            for (i in dbManager.readDbPlayersTimerCounter()) {
                timerText?.append("$i\n")
            }
            dbManager.closeDb()
            onStart()
        }
    }
}