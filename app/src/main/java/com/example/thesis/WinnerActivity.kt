package com.example.thesis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class WinnerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_winner)

        val duel = Intent(this, Duel::class.java)
        val main = Intent(this, MainActivity::class.java)

        val btn: Button = findViewById(R.id.button)
        val btn2: Button = findViewById(R.id.button2)

        val data1 = intent.extras?.getString("1")
        val data2 = intent.extras?.getString("2")

        val tv: TextView = findViewById(R.id.textView)

        if (data1 != null)
            tv.text = "$data1 WIN"
        else tv.text = "$data2 WIN"

        btn.setOnClickListener {
            onDestroy()
            startActivity(duel)
        }

        btn2.setOnClickListener {
            onDestroy()
            startActivity(main)
        }

    }
}