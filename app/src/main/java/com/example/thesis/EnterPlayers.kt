package com.example.thesis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.thesis.db.DbManager

class EnterPlayers : AppCompatActivity() {

    val dbManager = DbManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.enter_players)

        val btn_add: Button = findViewById(R.id.btnadd)
        val btn_play: Button = findViewById(R.id.btnplay)
        val btn_delete: Button = findViewById(R.id.btndelete)
        val editPersonName: EditText = findViewById(R.id.editTextPersonName)
        val textPlayers: TextView = findViewById(R.id.textPlayers)

        val mainActivity = Intent(this, MainActivity::class.java)

        dbManager.openDb()
        if (dbManager.readDbPlayersNames().size == 0) {
            textPlayers.text = "Пока нет игроков"
        } else {
            for (i in dbManager.readDbPlayersNames()) {
                textPlayers.append(i)
                textPlayers.append("\n")
            }
        }

        btn_add.setOnClickListener {
            btn_add.text = "Добавить"
            textPlayers.text = ""
            if (editPersonName.text.isEmpty()) {
                editPersonName.hint = "ВВЕДИ ИМЯ, БЛЯДЬ!"
                for (i in dbManager.readDbPlayersNames()) {
                    textPlayers.append(i)
                    textPlayers.append("\n")
                }
            } else {
                dbManager.insertPlayerToDb(editPersonName.text.toString(), 0, 0)
                for (i in dbManager.readDbPlayersNames()) {
                    textPlayers.append(i)
                    textPlayers.append("\n")
                }
                editPersonName.text = null
                editPersonName.hint = "Введите имя"
            }
        }

        btn_play.setOnClickListener {
            startActivity(mainActivity)
        }

        btn_delete.setOnClickListener {
            dbManager.removeAll()
            textPlayers.text = ""
        }
    }

    override fun onPause() {
        super.onPause()
        dbManager.closeDb()
    }

    override fun onResume() {
        super.onResume()
        dbManager.openDb()
    }

    override fun onStart() {
        super.onStart()
        dbManager.openDb()

    }
}